faker (33.3.1-1) unstable; urgency=medium

  * Team upload.
  * [6502c23] New upstream version 33.3.1

 -- Douglas Alves dos Santos <douglasvestos@gmail.com>  Tue, 14 Jan 2025 09:58:17 -0300

faker (33.1.0-1) unstable; urgency=medium

  * Team upload.
  * [bf3b531] New upstream version 33.1.0
  * [bde26ba] d/rules: Remove .mypy_cache while dh_clean

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 15 Dec 2024 09:37:45 +0200

faker (28.1.0-2) unstable; urgency=medium

  * Team upload.
  [ Carsten Schoenert ]
  * [fcff057] Rebuild patch queue from patch-queue branch
    docs-Remove-potential-privacy-breaking-external-linking.patch

  [ Kathara Sasikumar ]
  * Source only Upload

 -- Kathara Sasikumar <katharasasikumar007@gmail.com>  Tue, 03 Sep 2024 14:01:40 +0530

faker (28.1.0-1) unstable; urgency=medium

  * Team upload.
  [ Carsten Schoenert ]
  * [db8ecdd] d/gbp.conf: Add some default data
  * [60adf75] d/README.source: Add some defaults hints

  [ Kathara Sasikumar ]
  * [8fe64d0] New upstream version 28.1.0
  * [eeb923d] d/control: Add faker-doc package
    (Closes: #1077787)
  * [afb8fe8] d/control: Update/sort Build-Depends for faker-doc package
  * [f917844] d/rules: Add doc variables and override dh_sphinxdoc
  * [473bf53] d/faker-doc.doc-base: Add new needed sequencer
  * [f70cff8] Rebuild patch queue from patch-queue branch
    Added patch:
    docs-Use-packaged-intersphinx-resources.patch
  * [60301fe] d/README.source: Fixed directory name

 -- Kathara Sasikumar <katharasasikumar007@gmail.com>  Mon, 02 Sep 2024 12:32:05 +0530

faker (26.0.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 26.0.0
  * Bump Standards-Version to 4.7.0
    No further modifications needed.

 -- Kathara Sasikumar <katharasasikumar007@gmail.com>  Mon, 01 Jul 2024 23:50:43 +0530

faker (24.4.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 24.4.0
  * Remove extraneous dependency on python3-mock
  * Use new dh-sequence-python3

 -- Alexandre Detiste <tchet@debian.org>  Thu, 28 Mar 2024 01:19:10 +0100

faker (22.0.0-1) unstable; urgency=medium

  * Team upload.
  * Delete old debian/patches and add skip tests patch.
  * New upstream version 22.0.0. (Closes: #952388)
  * Update d/control.
    - Set std-ver to 4.6.2.
    - Add Rules-Requires-Root.
    - Add dependencies on python3-dateutil and python3-pil to the
      binary package.
  * Update d/copyright.
  * Set standard to 4 on d/watch.
  * Running test with pytest and add autopkgtest

 -- Bo YU <tsu.yubo@gmail.com>  Tue, 02 Jan 2024 15:10:20 +0000

faker (0.9.3-2) unstable; urgency=medium

  * Team upload.
  * Add conflicts with old ruby-faker (#1011443)

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 06 Jul 2022 08:40:59 +0200

faker (0.9.3-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Sandro Tosi <morph@debian.org>  Sun, 12 Jun 2022 21:35:15 -0400

faker (0.9.3-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream version 0.9.3
  * drop patches: obsolete
  * add patch to use unidecode (not text_unidecode);
    document in copyright file that effective license is now GPL-2+;
    build-depend on python3-unidecode
  * add patches to avoid modules ukpostcodeparser email_validator
    in tests
  * build-depend on python3-pytest python3-pytest-runner

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 29 Mar 2020 23:01:48 +0200

faker (0.7.7-3) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * Use debhelper-compat instead of debian/compat.

  [ Andrey Rahmatullin ]
  * Drop Python 2 support.

 -- Andrey Rahmatullin <wrar@debian.org>  Fri, 16 Aug 2019 00:26:24 +0500

faker (0.7.7-2) unstable; urgency=high

  * Fix Non-determistically FTBFS due to tests sometimes exposing
    UnboundLocalError. Closes: #849333.
  * Remove century tests. Closes: #849652.

 -- Brian May <bam@debian.org>  Tue, 31 Jan 2017 17:32:59 +1100

faker (0.7.7-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 3.9.8.
  * debian/.git-dpm: fix patchedTag format.
  * Remove upstream applied patch 0001-Improve-command-line-output.patch.

 -- Hugo Lefeuvre <hle@debian.org>  Fri, 23 Dec 2016 18:50:32 +0100

faker (0.5.7-1) unstable; urgency=low

  * Initial release (Closes: #814045)

 -- Christopher Baines <mail@cbaines.net>  Sat, 19 Mar 2016 15:32:52 +0000
